{
  description = "A Nix-flake-based Go development environment";

  inputs.nixpkgs = { url = "github:NixOS/nixpkgs/nixos-23.11"; };

  outputs = {
    self,
    nixpkgs,
  }: let
    goVersion = 21; # Change this to update the whole stack
    overlays = [(final: prev: {go = prev."go_1_${toString goVersion}";})];
    supportedSystems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];
    forEachSupportedSystem = f:
      nixpkgs.lib.genAttrs supportedSystems (system:
        f {
          pkgs = import nixpkgs {inherit overlays system;};
        });
  in {
    devShells = forEachSupportedSystem ({pkgs}: {
      default = pkgs.mkShell {
        packages = with pkgs; [
          go
          gotools
          gopls
          golangci-lint
        ];
      };
    });
  };
}
