{
  description = "My collection of nix flake templates";

  outputs = { self }: {

    templates = {

      zig = {
        path = ./zig;
        description = "A basic template for zig development";
      };

      odin = {
        path = ./odin;
        description = "A basic template for odin development";
      };

      go = {
        path = ./go;
        description = "A basic template for go development";
      };

      rust = {
        path = ./rust;
        description = "A basic template for rust development";
      };

      python = {
        path = ./python;
        description = "A basic python poetry template";
      };

      ocaml = {
        path = ./ocaml;
        description = "A basic ocaml template";
      };

      nix = {
        path = ./nix;
        description = "A template for developing a nix flake";
      };

      defaultTemplate = self.templates.zig;
    };
  };
}
