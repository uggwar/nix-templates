{
  description = "Nix Flake template for zig development";

  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/nixos-23.11"; };
    zig-overlay.url = "github:mitchellh/zig-overlay";
    zig-overlay.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils = { url = "github:numtide/flake-utils"; };
  };

  outputs = { self, nixpkgs, zig-overlay, flake-utils }:
  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system ; };
      zig = zig-overlay.packages.${system}.master;
    in
    with pkgs;
    {
      devShell = mkShell {
        buildInputs = [
        ];
        nativeBuildInputs = [ zig pkg-config ];
      };

      packages.default = pkgs.stdenv.mkDerivation {
        name = "game";
        version = "0.0.1";
        src = ./.;
        dontConfigure = true;
        dontInstall = true;
        doCheck = false;
        nativeBuildInputs = [ zig pkg-config ];
        buildPhase = ''
          mkdir -p $out/bin
          mkdir -p .cache
          zig build install --cache-dir $(pwd)/zig-cache --global-cache-dir $(pwd)/.cache --prefix $out
        '';
      };
    }
  );
}
